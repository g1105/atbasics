package ru.bitelecom.helper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Properties;

public class Utils {
    private static final Logger LOGGER = LogManager.getLogger(Utils.class);

    public static Properties loadPropertiesFromFile(String pathToFile) {
        File configFile = new File(pathToFile);
        Properties props = new Properties();
        try {
            FileInputStream input = new FileInputStream(configFile);
            props.load(new InputStreamReader(input, Charset.forName("UTF-8")));
            input.close();
        } catch (FileNotFoundException ex) {
            LOGGER.error("File does not exist: " + pathToFile);
        } catch (IOException ex) {
            LOGGER.error("I/O error: " + pathToFile);
        }
        return props;
    }

    public static void savePropertiesToFile(Properties props, String path) {
        File configFile = new File(path);
        try {
            FileOutputStream fos = new FileOutputStream((configFile));
            props.store(fos, "System info");
        } catch (FileNotFoundException ex) {
            LOGGER.error("File does not exist: " + path);
        } catch (IOException ex) {
            LOGGER.error("I/O error: " + path);
        }
    }
}