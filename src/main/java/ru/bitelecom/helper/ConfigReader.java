package ru.bitelecom.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ConfigReader {

    private static ConfigReader configReader;
    private final Map<String, String> CONFIG = new HashMap<>();


    private ConfigReader() {
        Properties properties = Utils.loadPropertiesFromFile("src/main/resources/config.properties");
        for (Map.Entry<Object, Object> pair : properties.entrySet()) {
            CONFIG.put((String) pair.getKey(), (String) pair.getValue());
        }
    }

    public static ConfigReader getConfigReader() {
        if (configReader == null) {
            configReader = new ConfigReader();
        }
        return configReader;
    }

    public Map<String, String> getConfig() {
        return CONFIG;
    }
}
