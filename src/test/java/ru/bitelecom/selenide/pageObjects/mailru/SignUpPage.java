package ru.bitelecom.selenide.pageObjects.mailru;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class SignUpPage {

    SelenideElement credentialFirstName = $x("//*[@name=\"fname\"]");
    SelenideElement credentialLastName = $x("//*[@name=\"lname\"]");
    SelenideElement birthdayDropdownBtn = $x("//div[@data-test-id=\"birth-date__day\"]");
    SelenideElement birthdayDropdownMenu = $x("//div[@data-test-id=\"birth-date__day__menu\"]");

    public SignUpPage enterCredentials(String fName, String lName) {
        credentialFirstName.should(Condition.visible).setValue(fName);
        credentialLastName.should(Condition.visible).setValue(lName);
        birthdayDropdownBtn.should(Condition.visible).click();
        birthdayDropdownMenu.should(Condition.visible).find("div[data-test-id=\"select-value:1\"]").find("span").click();
        return page(SignUpPage.class);
    }
}
