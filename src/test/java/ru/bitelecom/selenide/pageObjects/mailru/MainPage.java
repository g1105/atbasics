package ru.bitelecom.selenide.pageObjects.mailru;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.*;

public class MainPage {
    SelenideElement signUpBtn = $x("//a[@id=\"PH_regLink\"]");
    SelenideElement mailLogoImg = $x("//a[@href=\"https://mail.ru\"]/img[@src=\"https://img.imgsmail.ru/static.promo/logo/logo.svg\"]");
    SelenideElement promoVKSkipBtn = $x("//*[@class=\"vkc__EnterLogin__container\"]//button[@type=\"button\" and @class=\"vkc__PureButton__button vkc__Link__link vkc__Link__primary\"]");
    SelenideElement promoVKSkipBtn2 = $x("//*[@class=\"vkc__SkipLink__skipLink\"]");


    public MainPage openMailMainPage(String url) {
        open(url);
        mailLogoImg.should(Condition.visible);
        Assertions.assertTrue(mailLogoImg.isDisplayed(), "Логотип компании mail ru не появился на странице");
        return page(MainPage.class);
    }

    public SignUpPage goToSignUpPage() {
        signUpBtn.should(Condition.visible).click();
        //Если будет появляться рекламное окно входа Вконтакте
        if (promoVKSkipBtn.isDisplayed()) {
            promoVKSkipBtn.click();
        }
        if (promoVKSkipBtn2.isDisplayed()) {
            promoVKSkipBtn2.click();
        }

        return page(SignUpPage.class);
    }
}
