package ru.bitelecom.selenide.tests;

import org.junit.jupiter.api.Test;
import ru.bitelecom.helper.ConfigReader;
import ru.bitelecom.selenide.pageObjects.mailru.MainPage;

import java.util.Map;

public class MailSignUpTest extends BaseTest {

    public static Map<String, String> config = ConfigReader.getConfigReader().getConfig();

    @Test
    public void successEnterCredentials() {
        new MainPage().openMailMainPage("https://account.mail.ru/").
                goToSignUpPage().
                enterCredentials(config.get("first_name"), "last_name");
    }

}
